import { NgModule } from '@angular/core';
import { DeployPopoverComponent } from './deploy-popover/deploy-popover';
@NgModule({
	declarations: [DeployPopoverComponent],
	imports: [],
	exports: [DeployPopoverComponent]
})
export class ComponentsModule {}
