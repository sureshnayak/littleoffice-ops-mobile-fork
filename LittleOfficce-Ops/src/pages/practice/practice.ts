import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Base64 } from '@ionic-native/base64';
import { Geolocation} from '@ionic-native/geolocation';
import { NativeAudio } from '@ionic-native/native-audio';
import { Media, MediaObject } from '@ionic-native/media';
import { File } from '@ionic-native/file';

/**
 * Generated class for the PracticePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
/* const MEDIA_FILES_KEY = 'mediafiles';*/

declare var google;

@IonicPage()
@Component({
  selector: 'page-practice',
  templateUrl: 'practice.html',
})
export class PracticePage {

 /*  @ViewChild('myvideo') myvideo:any;
  mediaFiles = []; */

  user: any;
  lat: any;
  lang:any;

  recording: boolean = false;
  filePath: string;
  fileName: string;
  audio: MediaObject;
  audioList: any[] = [];
  audioFile: any;
  music: any;
  userLocations : number;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public nativeAudio: NativeAudio,
    public media: Media,
    public file: File,
    public base64: Base64,
    public api: ApiProvider,
    public geolocation: Geolocation) {
      /* this.getAudioList(); */
    this.geolocation.getCurrentPosition().then( pos => {
      let userLocations={
      lat : pos.coords.latitude,
      lang : pos.coords.longitude
    }
    console.log("locations are acquired")
    console.log(userLocations);
   this.lat= pos.coords.latitude;
    this.lang = pos.coords.longitude;
    }).catch(err=>console.log(err));
      /* this.platform.ready().then(() => {
        this.nativeAudio.preloadSimple('uniqueId1', 'assets/audio/raavana.mp3').then((success)=>{
        console.log("success");
        },(error)=>{
          console.log(error);
        });
      }); */
  }

  ionViewDidEnter(){
    this.user = new google.maps.Map(document.getElementById('map'),{
      center: { lat: -34.9011, lng: -56.1645 },
		  zoom: 15
    });
  }

  ionViewDidLoad() {

  }

  /* getAudioList() {
    if (localStorage.getItem("audiolist")) {
      this.audioList = JSON.parse(localStorage.getItem("audiolist"));
      console.log(this.audioList);
    }
  }

  startRecord() {
    if (this.platform.is('ios')) {
      this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.wav';
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    } else if (this.platform.is('android')) {
      this.fileName = 'record' + new Date().getDate() + new Date().getMonth() + new Date().getFullYear() + new Date().getHours() + new Date().getMinutes() + new Date().getSeconds() + '.mp3';
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + this.fileName;
      this.audio = this.media.create(this.filePath);
    }
    this.audio.startRecord();
    this.recording = true;
  }

  stopRecord() {
    this.audio.stopRecord();
    let data = { filename: this.fileName };
    this.audioList.push(data);
    localStorage.setItem("audiolist", JSON.stringify(this.audioList));
    this.recording = false;
    this.getAudioList();

    this.base64.encodeFile(this.filePath).then(
      (base64: any) => {
        alert('file base64 encoding: ' + base64);
        this.audioFile = base64.substr(34,base64.length);
        this.audioFile = "data:audio/mp3;base64," + this.audioFile;
        this.api.audioAdd('12',this.audioFile).then(res =>{
          alert('Add Audio');
          const data  = JSON.parse(res['_body']);
        });
      });
 */
  }

  /* playAudio(file, idx) {
    if (this.platform.is('ios')) {
      this.filePath = this.file.documentsDirectory.replace(/file:\/\//g, '') + file;
      this.audio = this.media.create(this.audioFile);
    } else if (this.platform.is('android')) {
      this.filePath = this.file.externalDataDirectory.replace(/file:\/\//g, '') + file;
      this.audio = this.media.create(this.music);
    }
    this.audio.play();
    this.audio.setVolume(0.8);
  } */

