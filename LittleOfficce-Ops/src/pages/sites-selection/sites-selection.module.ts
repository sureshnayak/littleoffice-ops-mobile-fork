import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SitesSelectionPage } from './sites-selection';

@NgModule({
  declarations: [
    SitesSelectionPage,
  ],
  imports: [
    IonicPageModule.forChild(SitesSelectionPage),
  ],
})
export class SitesSelectionPageModule {}
