import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the ForgotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot',
  templateUrl: 'forgot.html',
})
export class ForgotPage {
  user= {};
  phNo: string
  dpin: string;
  pass1: string = '';
  pass2: string = '';
  isValid = false;
  checkpass = false;
  next: number=1;
  email: string;
  userData: any;
  isuser = false;
  isTimer = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPage');
  }
  onClickNext1(){

  }
  onClickLogin(){
    this.navCtrl.push(HomePage);
  }
  resendPin(){

  }
  onClickNext(){

  }
}
