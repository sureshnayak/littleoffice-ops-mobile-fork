import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the ScheduleManagementPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedule-management',
  templateUrl: 'schedule-management.html',
})
export class ScheduleManagementPage {

  music: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    ) {

    this.api.audioRecord('1').then(res=>{

      const data = JSON.parse(res['_body']);
      this.music = data
      alert('Audio List Found');
    })

  }



}
