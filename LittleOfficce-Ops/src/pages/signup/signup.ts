import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Camera } from '@ionic-native/camera';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  user = {};
  email: string;
  mobile: string;
  otp: string;
  pass1: string;
  pass2: string;
  photoUrl: string;
  userData: any;
  next: number = 1;
  isValid = false;
  isuser = false;
  checkpass = false;
  isphoto = false;
  isTimer = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,public camera: Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }
  onClickLogin() {
    this.navCtrl.push(HomePage);
  }
  resendPin(){

  }
  onClickNext1(){

  }
  onClickGetPic(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 320,
      targetHeight: 320
    }).then((data) => {
      this.photoUrl = "data:image/jpeg;base64," + data;

    }, (error) => {

    });
  }
  onClickNext(){

  }
}
