import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the OthersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-others',
  templateUrl: 'others.html',
})
export class OthersPage {
  timerVar: any;
  timerVal: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.startTimer();
  }
  startTimer(){
    this.timerVar=Observable.interval(1000).subscribe(x =>{
      this.timerVal = x;
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OthersPage');
  }

}
